package com.dev.restaurant.configs;

import java.util.Collections;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;

import com.dev.restaurant.constants.KafkaConstants;

import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.SenderOptions;

@Configuration
public class KafkaConfig {

	@Bean
    public ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate(
            KafkaProperties properties) {
        Map<String, Object> props = properties.buildProducerProperties();
        return new ReactiveKafkaProducerTemplate<String, String>(SenderOptions.create(props));
    }
	
	/*----------------------------consumers----------------------------------*/
	
	/* ----------------------new order consumer------------------------------*/
	@Bean(name="NewOrderReceiverOptions")
    public ReceiverOptions<String, String> kafkaNewOrderReceiverOptions(KafkaProperties kafkaProperties) {
		Map<String, Object> props = kafkaProperties.buildConsumerProperties();
		/*this is kafka's default consumer group , create new in production*/		
		props.put(ConsumerConfig.GROUP_ID_CONFIG , "test-consumer-group");
		ReceiverOptions<String, String> basicReceiverOptions = ReceiverOptions.create(props);
        return basicReceiverOptions.subscription(Collections.singletonList(KafkaConstants.NEW_ORDER));
    }
	
    @Bean(name="NewOrderKafkaTemplate")
    public ReactiveKafkaConsumerTemplate<String, String> reactiveKafkaConsumerTemplate(@Qualifier("NewOrderReceiverOptions") ReceiverOptions<String, String> kafkaNewOrderReceiverOptions) {
        return new ReactiveKafkaConsumerTemplate<String, String>(kafkaNewOrderReceiverOptions);
    }
    
}
