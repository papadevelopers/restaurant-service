package com.dev.restaurant.service;

import java.sql.Timestamp;
import java.time.Duration;
import java.util.Calendar;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dev.restaurant.constants.KafkaConstants;
import com.dev.restaurant.constants.OrderStatus;
import com.dev.restaurant.entities.RestaurantOrder;
import com.dev.restaurant.repositories.RestaurantOrderRepository;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class RestaurantOrderService {

	@Autowired
	private Gson gson;

	@Autowired
	private ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate;
	
	@Autowired
	private RestaurantOrderRepository repository;

	@Transactional
	public Mono<RestaurantOrder> createNewRestaurantOrder(String orderJson) {
		RestaurantOrder restaurantOrder = new RestaurantOrder();
		JSONObject order = gson.fromJson(orderJson, JSONObject.class);
		restaurantOrder.setOrderId(order.getAsString("id"));
		restaurantOrder.setStatus(OrderStatus.RESTAURANT_PREPAIRING.name());
		restaurantOrder.setCreatedAt(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		return this.repository.save(restaurantOrder);
	}

	public RestaurantOrder createFailedRestaurantOrder(String error) {
		RestaurantOrder restaurantOrder = new RestaurantOrder();
		restaurantOrder.setStatus(OrderStatus.RESTAURANT_FAILED.name());
		restaurantOrder.setCreatedAt(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		restaurantOrder.setReason(error);
		return restaurantOrder;
	}
	
	public Mono<RestaurantOrder> save(RestaurantOrder restaurantOrder) {
		return this.repository.save(restaurantOrder);
	}

	/**
	 * Send restaurant order for payment
	 * 
	 * @param restaurantOrder
	 */
	@Transactional
	public void createNewPaymentOrder(final RestaurantOrder restaurantOrder) {
		reactiveKafkaProducerTemplate.send(KafkaConstants.NEW_ORDERPAYMENT, gson.toJson(restaurantOrder)).subscribe();
	}

	public Mono<RestaurantOrder> processOrder(String orderJson) {
		return createNewRestaurantOrder(orderJson)
			.doOnNext(order-> reactiveKafkaProducerTemplate.send(KafkaConstants.RESTAURANT_ORDER_STATUS , gson.toJson(order)).subscribe())
			.delayElement(Duration.ofSeconds(5L)) // BACKPRESSURE
			.doOnSuccess(order-> {
				if (new Random().nextBoolean()) { 
					log.info("Order preparation success for {}",order); 
				 	order.setStatus(OrderStatus.RESTAURANT_CREATED.name()); // no need to register
				 	reactiveKafkaProducerTemplate.send(KafkaConstants.RESTAURANT_ORDER_STATUS , gson.toJson(order)).subscribe();
				 	save(order).doOnSuccess(orderDB-> createNewPaymentOrder(order)).subscribe();
				 } else {
					 order.setStatus(OrderStatus.RESTAURANT_FAILED.name());
					 log.error("Order preparation failed for {}", order);
					 reactiveKafkaProducerTemplate.send(KafkaConstants.RESTAURANT_ORDER_STATUS , gson.toJson(order)).subscribe();
					 save(order).subscribe(); 
				}
			}).doOnError(throwable -> {
				log.error("error while saving restaurant order {}", throwable.getMessage());
			});
	}
}
