package com.dev.restaurant.handlers;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.reactive.ReactiveKafkaConsumerTemplate;
import org.springframework.kafka.core.reactive.ReactiveKafkaProducerTemplate;
import org.springframework.stereotype.Component;

import com.dev.restaurant.constants.KafkaConstants;
import com.dev.restaurant.service.RestaurantOrderService;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Component
@EnableKafka
@Slf4j
public class RestaurantHandler {

	private final ReactiveKafkaConsumerTemplate<String, String> reactiveKafkaConsumerTemplate;

	private final ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate;

	private final RestaurantOrderService service;

	private final Gson gson;

	public RestaurantHandler(@Qualifier("NewOrderKafkaTemplate") ReactiveKafkaConsumerTemplate<String, String> reactiveKafkaConsumerTemplate,
			ReactiveKafkaProducerTemplate<String, String> reactiveKafkaProducerTemplate, RestaurantOrderService service,
			Gson gson) {
		super();
		this.reactiveKafkaConsumerTemplate = reactiveKafkaConsumerTemplate;
		this.reactiveKafkaProducerTemplate = reactiveKafkaProducerTemplate;
		this.service = service;
		this.gson = gson;

		processNewOrder().subscribe();
	}

	public Flux<String> processNewOrder() {
		return reactiveKafkaConsumerTemplate.receiveAutoAck()
				.doOnNext(consumerRecord -> log.info("received new Order key={}, value={} from topic={}, offset={}",
						consumerRecord.key(), consumerRecord.value(), consumerRecord.topic(), consumerRecord.offset()))
				.map(ConsumerRecord::value)
				.doOnNext(msg -> service.processOrder(msg).subscribe())
				.doOnError(throwable -> {
					log.error("something bad happened while processing order: {}", throwable.getMessage());
					reactiveKafkaProducerTemplate.send(KafkaConstants.RESTAURANT_ORDER_STATUS,
							gson.toJson(service.createFailedRestaurantOrder(throwable.getMessage()))).subscribe();
				});
	}
}
