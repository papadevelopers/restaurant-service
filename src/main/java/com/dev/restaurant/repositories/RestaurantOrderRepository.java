package com.dev.restaurant.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.dev.restaurant.entities.RestaurantOrder;

public interface RestaurantOrderRepository extends ReactiveCrudRepository<RestaurantOrder, String>{

}
