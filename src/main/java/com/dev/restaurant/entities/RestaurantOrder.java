package com.dev.restaurant.entities;

import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("restaurant_orders")
public class RestaurantOrder{
	@Id private UUID id;
	private String orderId;
	private String status;
	private Timestamp createdAt; 
	private String reason;
}