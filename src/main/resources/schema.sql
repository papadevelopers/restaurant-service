CREATE TABLE public.restaurant_orders (
	id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
	order_id varchar NULL,
	status varchar NOT NULL,
	created_at timestamp NULL,
	reason varchar NULL
);